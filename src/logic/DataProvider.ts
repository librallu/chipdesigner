/* eslint-disable */
import { Step1Data, Step2Data, Step3Data } from "./structures"

/**
  Singleton element for stocking data
*/
export namespace DataProvider {

    let step3Data: Step3Data
    let step2Data: Step2Data
    let step1Data: Step1Data
    let debug3: any
    let observers: any = []
    let obs_step1: any = []
    let obs_step2: any = []
    let obs_step3: any = []

    function callObservers1() {
        obs_step1.forEach( (e: any) => { e() })
    }

    function callObservers2() {
        obs_step2.forEach( (e: any) => { e() })
    }

    function callObservers3() {
        obs_step3.forEach( (e: any) => { e() })
    }

    function callObservers() {
        observers.forEach( (e: any) => {
        e()
        })
    }

    export function initData(s1: Step1Data, s2: Step2Data, s3: Step3Data): void {
        step1Data = s1
        step2Data = s2
        step3Data = s3
        callObservers()
    }

    export function initStep3Data(s: Step3Data, update?: boolean): void {
        step3Data = s
        callObservers()
        callObservers2()
        callObservers3()
    }
    export function initStep2Data(s: Step2Data): void {
        step2Data = s
        callObservers()
        callObservers1()
        callObservers3()
    }
    export function initStep1Data(s: Step1Data): void {
        step1Data = s
        callObservers()
        callObservers2()
        callObservers1()
    }
    export function initDebug3Data(s: any): void { debug3 = s }

    export function addChangeObserver(e: any): void { observers.push(e) }
    export function addObserver1(e: any): void { obs_step1.push(e) }
    export function addObserver2(e: any): void { obs_step2.push(e) }
    export function addObserver3(e: any): void { obs_step3.push(e) }

    export function getStep1Data() { return step1Data }
    export function getStep2Data() { return step2Data }
    export function getStep3Data() { return step3Data }
    export function getDebug3Data() { return debug3 }

    export function getNodeIdFromName(s: string): number {
        let res = -1
        step1Data.nodes.forEach( (e,i) => {
            if ( e.name === s )
                res = Number(i)
        })
        return res
    }

}
