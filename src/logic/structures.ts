/* eslint-disable */
import { Circle, Rectangle } from "./geometry/Collisions"
import { Coord } from "./geometry/Coord"

/**
  EXPORTS:
    - interface Inlet
    - interface Outlet
    - interface Node
    - interface Chip
    - enum ChannelDirection
    - function getNodeFromName
*/

export interface Inlet extends Circle {

}

export interface Outlet extends Circle {

}

export interface Node {
  name: string;
}

export interface Chip {
  w: number;
  h: number;
  name: string;
}

export enum ChannelDirection {
  Horizontal,
  Vertical,
}


export function channelDirectionToString(d:ChannelDirection) {
  if ( d === ChannelDirection.Horizontal )
    return "Horizontal"
  else
    return "Vertical"
}


/**
  Step 3 types
*/
export interface Step3Node extends Node {
  inlet: Coord;
  outlet: Coord;
}

interface Step3PathInstruction {
  type: string;
}

interface Step3InstructionSegment extends Step3PathInstruction {
  x1: number;
  x2: number;
  y1: number;
  y2: number;
}

export interface Step3Channel {
  in: Coord[];
  out: Coord[];
  name: string;
  dimentions?: {
    [propName: string]: number;
  };
}

export interface Step3Data {
  nodes: Step3Node[];
  params: {
    minRadius:number;
    inletRadius:number;
    outletRadius:number;
    precision: number;
    minDistChamber: number;
    minDistNode: number;
    distBorder: number;
    hPunchHoles: number;
    hChamber: number;
  };
  channels: Step3Channel[];
}


/**
  Step 2 Types
*/
export interface Step2Node extends Node, Rectangle {
}

export interface Step2Channels extends Rectangle {
  dir: ChannelDirection;
  from: string;
  to: string;
  x: number;
  y: number;
  w: number;
  h: number;
  length: number;
  width: number;
}

export interface Step2Data {
  nodes: Step2Node[];
  channelPositions: Step2Channels[];
}


/**
  Step 1 Types
*/
interface Step1Node extends Node {
  neurons: number;
  surface: number;
}

interface Step1Channel {
  from: string;
  to: string;
  connectivity: number;
  type: string;
  length: number;
  width: number;
}

export interface Step1Data {
  nodes: Step1Node[];
  channels: Step1Channel[];
  chip: Chip;
}
