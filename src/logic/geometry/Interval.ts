import { Coord } from "./Coord"

/**
  returns true if x in [a,b]
*/
export function inInterval(x: number, a: number, b: number): boolean {
  return x > Math.min(a,b) && x < Math.max(b,a)
}

/**
  returns true if two intervals ab, cd overlap
*/
export function isOverlap(a: number,b: number,c: number,d: number): boolean {
  return Math.max(a,c) <= Math.min(b,d)
}

/**
  returns true iff two segments ab, cd, intersects (overlap or intersects)
*/
export function intersect(a: Coord, b: Coord, c: Coord, d: Coord): boolean {
  // check if ab intersects uv
  if ( Math.abs(a.y-b.y) > 0 && Math.abs(c.x-d.x) > 0 ) { // ab vertical, uv horizontal
    return inInterval(a.x,c.x,d.x) || inInterval(c.y,a.y,b.y)
  }
  else if ( Math.abs(a.x-b.x) > 0 && Math.abs(c.y-d.y) > 0 ) { // ab horizontal, uv vertical
    return inInterval(a.y,c.y,d.y) || inInterval(c.x,a.x,b.x)
  }
  // check if ab overlaps uv
  else if ( Math.abs(a.y-b.y) > 0 && Math.abs(c.y-d.y) > 0 && a.x === c.x ) { // both vertical and same x
    return isOverlap(a.y,b.y,c.y,d.y)
  }
  else if ( Math.abs(a.x-b.x) > 0 && Math.abs(c.x-d.x) > 0 && a.y === c.y ) { // both horizontal and same y
      return isOverlap(a.x,b.x,c.x,d.x)
  } else {
    return true;
  }
}
