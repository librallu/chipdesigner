/* eslint-disable */

import { Coord } from './Coord'

export interface Vector {
  x:number,
  y:number
}

/**
  returns direction of vec a,b
*/
export function getDirection(a:Coord, b:Coord, epsilon=0.01): Vector {
  let v = {x: b.x-a.x, y: b.y-a.y}
  return v
}

export function dotProduct(a:Vector, b:Vector):number {
  return a.x*b.x+a.y*b.y
}

export function norm(v:Vector):number {
  return Math.sqrt(v.x**2 + v.y**2)
}

/**
returns true if directions are the same
*/
export function sameDirection(a:Vector,b:Vector, epsilon=0.01): boolean {
  return Math.abs(dotProduct(a,b)-(norm(a)*norm(b))) < epsilon
}
