import { inInterval } from "./Interval"


export interface Circle {
  x: number;
  y: number;
  r: number;
}

export interface Rectangle {
  x: number;
  y: number;
  w: number;
  h: number;
}


/**
  returns true iff rectangle a and circle b overlap
  /!\ APPROXIMATION, some cases are not covered
*/
export function overlapRectangleCircle(a: Rectangle, b: Circle): boolean {
  return (b.x-a.x-a.w)**2+(b.y-a.y-a.h)**2 <= b.r**2 ||
    (b.x-a.x-a.w)**2 + (b.y-a.y)**2 <= b.r**2 ||
    (b.x-a.x)**2 + (b.y-a.y-a.h)**2 <= b.r**2 ||
    (b.x-a.x)**2 + (b.y-a.y)**2 <= b.r**2;
}

/**
  returns true if two rectangles overlap
*/
export function rectanglesOverlap(a: Rectangle, b: Rectangle): boolean {
  const xoverlap = inInterval(a.x, b.x,b.x+b.w) || inInterval(b.x, a.x, a.x+a.w);
  const yoverlap = inInterval(a.y, b.y,b.y+b.h) || inInterval(b.y, a.y, a.y+a.h);
  return xoverlap && yoverlap;
}
