/* eslint-disable */
import { DataProvider } from "../DataProvider"
import { ChannelDirection } from "../structures"

export interface Face {
  name:string,
  x1:number,
  x2:number
  y: number
}

enum Type {
  Start, End
}

interface Poi {
  x: number,
  type: Type,
  name: string
}

export function sweepLine(faces:Face[], a:Face, b:Face, epsilon:number):{x1:number, x2:number}[] {
  // eliminate faces not between a and b
  let faces_inter = faces.filter((e) => {return e.y > Math.min(a.y,b.y) && e.y < Math.max(a.y,b.y)})
  faces_inter = faces_inter.filter((e) => { return e.x1 < Math.min(a.x2,b.x2) })
  faces_inter = faces_inter.filter((e) => {return e.x2 > Math.max(a.x1,b.x1)})
  // sort faces
  let pois:any[] = []
  faces_inter.forEach( (e) => { pois.push({x:e.x1, type:Type.Start, name:e.name}) ; pois.push({x:e.x2, type:Type.End, name:e.name}) })
  pois.sort((a,b)=>{return a.x-b.x})
  let sol = [Math.max(a.x1,b.x1)]
  let current:any[] = []
  pois.forEach( (e) => {
    if ( e.type === Type.Start ) {
      if ( current.length === 0 ) {
        sol.push(e.x) // end solution interval
      }
      current.push(e.name)
    } else if ( e.type === Type.End ) {
      current = current.filter( (f) => {return f!==e.name})
      if ( current.length === 0 ) {
        sol.push(e.x) // start new solution interval
      }
    }
  })
  sol.push(Math.min(a.x2,b.x2))
  // create result (intervals)
  let res = []
  for ( let i = 0 ; i < sol.length ; i+=2 ) {
    res.push({ x1:sol[i], x2:sol[i+1] })
  }
  // remove intervals < epsilon (15µm)
  res = res.filter( (e) => {
    return e.x2-e.x1 > epsilon
  })
  return res
}


export function computeChannels() {
  let epsilon = 15e-3
  let step1Data = DataProvider.getStep1Data()
  let step2Data = DataProvider.getStep2Data()
  step2Data.channelPositions = []
  for ( var i = 0 ; i < step1Data.channels.length ; i++ ) {

    // for all channnels, compute list of faces between them
    let from = step2Data.nodes[DataProvider.getNodeIdFromName(step1Data.channels[i].from)]
    let to = step2Data.nodes[DataProvider.getNodeIdFromName(step1Data.channels[i].to)]

    let dir = "vertical"
    let x = 0
    let y = 0
    let w = 0
    let h = 0
    let rects:any[] = []

    if ( from.x+from.w < to.x ) {
      x = from.x+from.w
      y = Math.max(from.y,to.y)
      w = to.x-from.x-from.w
      h = Math.max(0, Math.min(from.y+from.h,to.y+to.h)-Math.max(from.y,to.y))
      if ( w > 0 && h > 0 ) {
        dir="horizontal"
        let face_a = {name:from.name, x1:from.y, x2:from.y+from.h, y:from.x+from.w}
        let face_b = {name:to.name, x1:to.y, x2:to.y+to.h, y:to.x}
        let faces:Face[] = step2Data.nodes.map( (e) => {
          return {name:e.name, x1:e.y, x2:e.y+e.h, y: e.x}
        })
        let rects_tmp = sweepLine(faces, face_a, face_b, epsilon)
        rects = rects_tmp.map( (e) => {
          return { x:from.x+from.w, y:e.x1, w:to.x-from.x-from.w, h:e.x2-e.x1}
        })
      }
    } else if ( to.x+to.w < from.x ) {
      x = to.x+to.w
      y = Math.max(to.y,from.y)
      w = from.x-to.x-to.w
      h = Math.max(0, Math.min(to.y+to.h,from.y+from.h)-Math.max(to.y,from.y))
      if ( w > 0 && h > 0 ) {
        dir="horizontal"
        let face_a = {name:from.name, x1:from.y, x2:from.y+from.h, y:from.x}
        let face_b = {name:to.name, x1:to.y, x2:to.y+to.h, y:to.x+to.w}
        let faces:Face[] = step2Data.nodes.map( (e) => {
          return {name:e.name, x1:e.y, x2:e.y+e.h, y: e.x}
        })
        let rects_tmp = sweepLine(faces, face_b, face_a, epsilon)
        rects = rects_tmp.map( (e) => {
          return { x:to.x+to.w, y:e.x1, w:from.x-to.x-to.w, h:e.x2-e.x1}
        })
      }
    } else if ( from.y+from.h < to.y ) {
      x = Math.max(from.x, to.x)
      y = from.y+from.h
      w = Math.max(0, Math.min(from.x+from.w,to.x+to.w)-Math.max(from.x,to.x))
      h = to.y-from.y-from.h
      if ( w > 0 && h > 0 ) {
        dir="vertical"
        let face_a = {name:from.name, x1:from.x, x2:from.x+from.w, y:from.y+from.h}
        let face_b = {name:to.name, x1:to.x, x2:to.x+to.w, y:to.y}
        let faces:Face[] = step2Data.nodes.map( (e) => {
          return {name:e.name, x1:e.x, x2:e.x+e.w, y: e.y}
        })
        let rects_tmp = sweepLine(faces, face_a, face_b, epsilon)
        rects = rects_tmp.map( (e) => {
          return { x:e.x1, y:from.y+from.h, w:e.x2-e.x1, h:to.y-from.y-from.h}
        })
      }
    } else if ( to.y+to.h < from.y ) {
      x = Math.max(to.x, from.x)
      y = to.y+to.h
      w = Math.max(0, Math.min(to.x+to.w,from.x+from.w)-Math.max(to.x,from.x))
      h = from.y-to.y-to.h
      if ( w > 0 && h > 0 ) {
        dir="vertical"
        let face_a = {name:from.name, x1:from.x, x2:from.x+from.w, y:from.y}
        let face_b = {name:to.name, x1:to.x, x2:to.x+to.w, y:to.y+to.h}
        let faces:Face[] = step2Data.nodes.map( (e) => {
          return {name:e.name, x1:e.x, x2:e.x+e.w, y: e.y}
        })
        let rects_tmp = sweepLine(faces, face_b, face_a, epsilon)
        rects = rects_tmp.map( (e) => {
          return { x:e.x1, y:to.y+to.h, w:e.x2-e.x1, h:from.y-to.y-to.h}
        })
      }
    }
    rects.forEach( (e) => {
      step2Data.channelPositions.push({
        dir: dir=="horizontal"?ChannelDirection.Horizontal:ChannelDirection.Vertical,
        from: step1Data.channels[i].from,
        to: step1Data.channels[i].to,
        x: e.x,
        y: e.y,
        w: e.w,
        h: e.h,
        length: dir=="horizontal"?e.w:e.h,
        width: dir=="horizontal"?e.h:e.w
      })
    })
  }
}

export interface Step2Eval {
    channels: {from:string, to:string, width:number, length:number}[]
  }
  
  export function step2Evaluation():Step2Eval {
    let s1Data = DataProvider.getStep1Data()
    let s2Data = DataProvider.getStep2Data()
  
    // compute channel length
    const channels = s1Data.channels.map((e) => {
      let res = 0
      let length = 0
      s2Data.channelPositions.forEach( (c) => {
        if ( e.from+"-"+e.to === c.from+"-"+c.to ) {
          res += c.width
          length = c.length
        }
      })
      return {from: e.from, to: e.to, width:res, length:length, idealLength:e.length}
    })
  
    return {
      channels: channels
    }
  }