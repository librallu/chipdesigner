import { ChannelConstraints, ChannelDimensions, evalDimensions } from "./eval"

/**
 * Step size for each channel dimension (expressed in mm)
 */
export const discretization = {
    w: 50e-3,
    h: 50e-3,
    l: 1.
}

export interface ChannelSearchSol {
    sol: ChannelDimensions;
    eval: number;
}

/**
 * brute force method that computes the best possible channel dimensions
 * @param csts channel constraints (min/max width/length/height of input/output channels)
 * @param lch chamber length
 * @param wch chamber width
 * @param hch chamber height (used to limit the maximal channel height)
 */
export function exhaustiveSearch(csts: ChannelConstraints, lch: number, wch: number, hch: number): ChannelSearchSol {
    let best = {lch:0., wch:0., lout:0., lin:0., win:0., wout:0., hin:0., hout:0.};
    let evalbest = 1e15;
    for ( let lin = csts.linMax ; lin >= csts.linMin ; lin -= discretization.l ) {
        for ( let lout = csts.loutMax ; lout >= csts.loutMin ; lout -= discretization.l ) {
            for ( let win = csts.winMax ; win >= csts.winMin ; win -= discretization.w ) {
                for ( let wout = csts.woutMax ; wout >= csts.woutMin ; wout -= discretization.w ) {
                    for ( let hin = csts.hinMin ; hin <= csts.hinMax ; hin += discretization.h ) {
                        for ( let hout = csts.houtMin ; hout <= Math.min(csts.houtMax, hch) ; hout += discretization.h ) {
                            const sol: ChannelDimensions = {lch: lch, wch: wch, lout: lout, lin: lin, win: win, wout: wout, hin: hin,hout: hout};
                            const evalsol = evalDimensions(sol, csts, lch, wch);
                            if ( evalsol < evalbest ) {
                                best = sol;
                                evalbest = evalsol;
                            }
                        }
                    }
                }
            }
        }
    }
    return {sol: best, eval: evalbest};
}