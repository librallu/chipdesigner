export function rounding(value: number, precision: number) {
    return Math.round(value/precision)*precision;
}

/**
 * Hardcoded microfluidic constants
 */
export const MICROFLUDIC_CONSTANTS = {
    vsed: .00000195,
    vadd: 4e-9,
    resin:2*Math.pow(10,-3),
    rho: 1000,
    g: 9.81,
    visco: 1e-3
}

/**
 * Channel dimensions (W x L x H for the inlet and the outlet)
 * These are obtained by solving some optimization problem.
 */
export interface ChannelDimensions {
    lch: number;
    wch: number;
    lout: number;
    lin: number;
    win: number;
    wout: number;
    hin: number;
    hout: number;
}
  
/**
 * Channel dimensions constraints
 */
export interface ChannelConstraints {
    winMin: number;
    winMax: number;
    hinMin: number;
    hinMax: number;
    linMin: number;
    linMax: number;
    woutMin: number;
    woutMax: number;
    houtMin: number;
    houtMax: number;
    loutMin: number;
    loutMax: number;
}


function tanh(x: number) {
    return (Math.exp(x) - Math.exp(-x))/(Math.exp(x) + Math.exp(-x));
}

  
export function computeLout(lch: number, wch: number, hin: number, win: number, lin: number, hout: number, wout: number) {
    // shortname for constants
    const vsed = MICROFLUDIC_CONSTANTS.vsed;
    const vadd = MICROFLUDIC_CONSTANTS.vadd;
    const resin = MICROFLUDIC_CONSTANTS.resin;
    const rho = MICROFLUDIC_CONSTANTS.rho;
    const g = MICROFLUDIC_CONSTANTS.g;
    const visco = MICROFLUDIC_CONSTANTS.visco;

    // rescale units (mm to m)
    lch /= 1000;
    wch /= 1000;
    hin /= 1000;
    win /= 1000;
    lin /= 1000;
    hout /= 1000;
    wout /= 1000;

    // ideal lout computation
    const vchmax = vsed*lch;
    const q = vchmax*wch;
    const deltaz = vadd/(Math.PI*Math.pow(resin,2));
    const rain = win/hin;
    const rout = wout/hout;
    const ain = 12*Math.pow(1-(192/(rain*Math.pow(Math.PI,5)))*tanh(Math.PI*rain/2),-1);
    const aout= 12*Math.pow(1-(192/(rout*Math.pow(Math.PI,5)))*tanh(Math.PI*rout/2),-1);
    let lout = (rho*g*deltaz/(q*visco) - ain*lin/(win*Math.pow(hin,3)) );
    lout *= wout*Math.pow(hout,3)/aout;
    return lout*1000;  // rescale from m to mm
}

  
/**
    Evaluation function for a channel dimension.
    It returns a number indicating how much the current solution deviates from a perfect dimentionning (0=perfect).
    Applies a penality if constraints are not respected.

    W_cin: discretization.W_cin/1000,
    W_cout: discretization.W_cout/1000,
    H_cin: discretization.H_cin/1000,
    H_cout: discretization.H_cout/1000
*/
export function evalDimensions(sol: ChannelDimensions, csts: ChannelConstraints, lch: number, wch: number): number {
    let res = 0;
    const epsilon = 1e-6;
    const amplitudeobj = 1e3; // scale to 1 micrometer (eval function must be <= 1)
    const amplitudebounds = 1e10; // penalty if constraints are not respected

    // check bounds
    { // win
        const bmin = csts.winMin;
        const bmax = csts.winMax;
        const v = sol.win;
        // if element not between min and max bounds: increase result
        res += (v>=bmin-epsilon&&v<=bmax+epsilon)?0:amplitudebounds;
    }
    { // wout
        const bmin = csts.woutMin;
        const bmax = csts.woutMax;
        const v = sol.wout;
        // if element not between min and max bounds: increase result
        res += (v>=bmin-epsilon&&v<=bmax+epsilon)?0:amplitudebounds;
    }
    { // hin
        const bmin = csts.hinMin;
        const bmax = csts.hinMax;
        const v = sol.hin;
        // if element not between min and max bounds: increase result
        res += (v>=bmin-epsilon&&v<=bmax+epsilon)?0:amplitudebounds;
    }
    { // hout
        const bmin = csts.houtMin;
        const bmax = csts.houtMax;
        const v = sol.hout;
        // if element not between min and max bounds: increase result
        res += (v>=bmin-epsilon&&v<=bmax+epsilon)?0:amplitudebounds;
    }

    // compute difference cost to ideal lout
    const idealLout = computeLout(lch, wch, sol.hin, sol.win, sol.lin, sol.hout, sol.wout);
    res += amplitudeobj*Math.abs(idealLout-sol.lout);

    return res;
}