/* eslint-disable */

import Swal from 'sweetalert2'

import { Chip, ChannelDirection, Step3Channel, Step2Node, Inlet } from "../../structures"
import { Coord } from "../../geometry/Coord"
import { prepRouting } from "./routingAlgorithm"
import { DataProvider } from "../../DataProvider"
import { evalDimensions } from "../eval"
import { exhaustiveSearch } from "../exhaustiveSearch"


function getNodeId(name:string):number {
  let s2Data = DataProvider.getStep2Data()
  let t = s2Data.nodes.filter(n => {return n.name === name})
  return t.length === 0 ? -1 : s2Data.nodes.indexOf(t[0])
}


/**
  returns the length and width of chamber according to the path of channels
*/
function getChamberDimentions(p_in:Coord[], n:Step2Node): {lch:number, wch:number} {
  let lch,wch;

  if (Math.abs(p_in[0].x-p_in[1].x) > 0.001) { // horizontal
    lch = n.w;
    wch = n.h;
  } else { // vertical
    lch = n.h;
    wch = n.w;
  }
  return {lch: lch, wch: wch}
}

function lengthPath(p:Coord[], n:Step2Node, c:Inlet, r:number):number {
  let res = 0;
  // count distances adapt with arc formulation
  let old = {x:-1,y:-1};
  for ( let i of p ) {
    if ( old.x == -1 ) old = i
    else {
      res += Math.abs(old.x-i.x)+Math.abs(old.y-i.y);
      old = i;
    }
  }

  // adapt for round angles
  let nb_turns = p.length-2;
  res += nb_turns*(r*Math.PI/2 - 2*r);

  // remove distance inside node
  if ( Math.abs(p[0].x-p[1].x) > 0 )
    res -= n.w/2;
  else
    res -= n.h/2;

  // remove distance inside inlet/outlet
  res -= c.r;

  return res;
}

export function createDimension(resInlet:{path:Coord[]}, resOutlet:{path:Coord[]}, name:string, acceptation_limit:number, op_type:string):boolean {
  let step2Data = DataProvider.getStep2Data()
  let step3Data = DataProvider.getStep3Data()
  let inlets = step3Data.nodes.map( (e) => { return {x:e.inlet.x, y:e.inlet.y, r:step3Data.params.inletRadius} })
  let outlets = step3Data.nodes.map( (e) => { return {x:e.outlet.x, y:e.outlet.y, r:step3Data.params.outletRadius} })
  // get dimentions of channel
  let p_in = resInlet.path
  let p_out = resOutlet.path
  let node_id = getNodeId(name)
  let n = step2Data.nodes[node_id]
  let ci = inlets[node_id]
  let co = outlets[node_id]
  let l_in = lengthPath(p_in,n,ci, step3Data.params.minRadius);
  let l_out = lengthPath(p_out,n,co, step3Data.params.minRadius);
  let lch = getChamberDimentions(p_in, n).lch
  let wch = getChamberDimentions(p_in, n).wch
  let hch = 500e-3;

    let csts = {
        winMin:200e-3,
        winMax: Math.min(wch, 1000e-3),
        hinMin: 50e-3,
        hinMax: Math.min(hch, 500e-3),
        linMin: l_in,
        linMax: l_in,
        woutMin: 200e-3,
        woutMax: Math.min(wch, 1000e-3),
        houtMin: 50e-3,
        houtMax: Math.min(hch, 500e-3),
        loutMin: l_out,
        loutMax: l_out,
    }


  let s = exhaustiveSearch(csts, lch, wch, hch)
  let eval_s = evalDimensions(s.sol, csts, lch, wch)
  if ( eval_s <= acceptation_limit) {
    step3Data.channels.push({
      in: resInlet.path,
      out: resOutlet.path,
      name: name,
      dimentions: {
        Lout: s.sol.lout,
        W_cin: s.sol.win,
        W_cout: s.sol.wout,
        L_cin: s.sol.lin,
        H_cin: s.sol.hin,
        H_cout: s.sol.hout,
      }
    })
    return true
  } else {
    return false
  }
}


/**
  - dir either Horizontal or Vertical
  - name: name of node ex 'cortex'
*/
export function route(name:string, dir:ChannelDirection, minDistPunchHole: number, minDistChannel: number) {

  // Load useful data for routing
  let step3Data = DataProvider.getStep3Data()
  let step2Data = DataProvider.getStep2Data()
  let chip:Chip = DataProvider.getStep1Data().chip

  // remove paths used by this node
  // console.debug(step3Data.channels)
  step3Data.channels = step3Data.channels.filter( (d:Step3Channel) => { return d.name !== name } );

  // try to construct path to inlet
  let inlets = step3Data.nodes.map( (e) => { return {x:e.inlet.x, y:e.inlet.y, r:step3Data.params.inletRadius} })
  let outlets = step3Data.nodes.map( (e) => { return {x:e.outlet.x, y:e.outlet.y, r:step3Data.params.outletRadius} })

  // TODO embed inlets, outlets,step2nodes, chip, step3channels into prepRouting instead of computing them before
  var resInlet = prepRouting(inlets, outlets, step2Data.nodes, chip, {
    node:name,
    type:'inlet',
    direction:dir
  }, step3Data.channels, minDistPunchHole, minDistChannel);

  // puts graph on the dataProvider debug object
  DataProvider.getDebug3Data().inputGraph = resInlet.graph

  var routingOK = false

  if ( resInlet.path.length > 0 ) {

    var resOutlet = prepRouting(inlets, outlets, step2Data.nodes, chip, {
      node:name,
      type:'outlet',
      direction:dir
    }, step3Data.channels, minDistPunchHole, minDistChannel, resInlet.path);

    // puts graph on the dataProvider debug object
    DataProvider.getDebug3Data().outputGraph = resOutlet.graph

    if ( resOutlet.path.length > 0 ) {
      routingOK = createDimension(resInlet, resOutlet, name, 100, "inlet first")
    }
  }

  if ( !routingOK ) { // try outlet then inlet

    var resOutlet = prepRouting(inlets, outlets, step2Data.nodes, chip, {
      node:name,
      type:'outlet',
      direction:dir
    }, step3Data.channels, minDistPunchHole, minDistChannel);

    // puts graph on the dataProvider debug object
    DataProvider.getDebug3Data().outputGraph = resOutlet.graph

    if ( resOutlet.path.length == 0 ) {
      Swal.fire('Oops','I am not able to find the path you require (for outlet).', 'error');
    } else {

      var resInlet = prepRouting(inlets, outlets, step2Data.nodes, chip, {
        node:name,
        type:'inlet',
        direction:dir
      }, step3Data.channels, minDistPunchHole, minDistChannel, resOutlet.path);

      // puts graph on the dataProvider debug object
      DataProvider.getDebug3Data().inputGraph = resInlet.graph

      if ( resInlet.path.length == 0 ) {
        Swal.fire('Oops','I am not able to find the path you require (for outlet).', 'error');
      } else {
        if ( !createDimension(resInlet, resOutlet, name, 100, "outlet first") ) {
          Swal.fire('No input/output channel dimention possible.', 'error')
        }
      }
    }
  }

}
