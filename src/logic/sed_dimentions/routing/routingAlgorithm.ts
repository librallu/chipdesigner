/* eslint-disable */
import { Inlet, Outlet, Step2Node, Chip, Step3Channel } from "../../structures"
import { Coord } from "../../geometry/Coord"
import { sameDirection, getDirection } from "../../geometry/Vector"
import { intersect, isOverlap } from "../../geometry/Interval"
import { Rectangle, Circle, overlapRectangleCircle, rectanglesOverlap } from "../../geometry/Collisions"
import { PathInstance, createGraph, Route } from "./approximationGraph"
import { DataProvider } from '../../DataProvider'

/**
  EXPORTS
   - function prepRouting: returns a graph & path for a routing sub-problem
*/

function minDistChamber() {
  return DataProvider.getStep3Data().params.minDistChamber
}

function precision() {
  return DataProvider.getStep3Data().params.precision
}

/**
  Generate starting and destination coordinates in the approximation graph
*/
function generateStartingPoints(inlet:Circle, node:Step2Node):PathInstance {
  return {
    sx: Math.floor((node.x+node.w/2)/precision()),
    sy: Math.floor((node.y+node.h/2)/precision()),
    tx: Math.floor((inlet.x)/precision()),
    ty: Math.floor((inlet.y)/precision())
  };
}

/**
  v: last point we want to add
  u: last point we added
  parent: parent[x][y] returns a coord
  returns: true if not intersecting with previous segments, false elsewhere
*/
function checkNotIntersecting(v:Coord, u:Coord, parent: Coord[][]):boolean {
  let p = {x:u.x, y:u.y}
  let l:Coord[] = []
  while(parent[p.y][p.x] !== p) {
    l.push({x:p.x, y:p.y})
    p = parent[p.y][p.x]
  }
  // if no previous segments, there is no intersection
  if ( l.length < 2 ) return true
  else {
    // check if new vector is in the same direction of previous one
    let w = parent[u.y][u.x]
    let duv = getDirection(u,v)
    let dwu = getDirection(w,u)
    if ( sameDirection(duv,dwu) ) return false

    for ( let i = 1 ; i < l.length ; i++ ) {
      let a = {x:l[i-1].x, y:l[i-1].y}
      let b = {x:l[i].x, y:l[i].y}
      if ( intersect(a,b,u,v) ) return false
    }
  }
  return true
}

/**
  returns number of hops from parent
*/
function nbHopsFromParent(c:Coord, parent:Coord[][]): number {
  let res = 0
  let u = {x: c.x, y: c.y}
  while(parent[u.y][u.x] !== u) {
    res++
    u = parent[u.y][u.x]
  }
  return res
}

function colorFromNbHops(n:number): string {
  if ( n == 1 ) return '#ff0000'
  else if ( n == 2 ) return '#cc3300'
  else if ( n == 3 ) return '#aa6600'
  else if ( n == 4 ) return '#779900'
  else if ( n == 5 ) return '#44cc00'
  else if ( n == 6 ) return '#11dd00'
  else return '#0000ff'
}

/**
  Take the approximation graph, an start and a destination, returns a path in this graph
*/
declare function step3_colorCases(instr:{x:number,y:number,color:string}[], prec:number):void;
declare function step3_drawApproximationGraph(res:boolean[][], prec:number):void;


/**
  Explore neighbors in the graph. Go from all 4 directions (up, down, left, right).
  Look if there are some nodes far enough (> min_dist) until there are no obstacle
*/
function exploreNeighbors(sx:number, sy:number, min_dist:number, g:boolean[][], parent:Coord[][], seen:boolean[][], remaining:Coord[]):void {
  // check if obstacle before
  const directions = [{x:-1, y:0}, {x:1, y:0}, {x:0, y:-1}, {x:0, y:1}] // top, bottom, left, right
  for ( let d of directions ) {
    let x = sx + d.x
    let y = sy + d.y
    while ( x>=0 && x<g[0].length && y>=0 && y<g.length && g[y][x] && !seen[y][x] ) {
      // test if there are something that blocks the path near the starting point
      if ( Math.abs(sx-x)+Math.abs(sy-y) > min_dist ) {
        parent[y][x] = {x:sx,y:sy}
        seen[y][x] = true
        remaining.push({x:x,y:y})
      }
      x += d.x
      y += d.y
    }
  }
}

/**
  From a given graph, returns a path in this graph
*/
function findPath(inst:PathInstance, g:boolean[][]):Coord[] {
  let res = [];
  let min_dist_turn = 4;
  let min_dist_turn_start = 4;

  // initialize temporary datas
  let parent:Coord[][] = [];
  let seen:boolean[][] = [];
  for ( let i = 0 ; i < g.length ; i++ ) {
    let tmp_parent = [];
    let tmp_seen = [];
    for ( let j = 0 ; j < g[i].length ; j++ ) {
      tmp_parent.push({x:-1, y:-1});
      tmp_seen.push(false);
    }
    parent.push(tmp_parent);
    seen.push(tmp_seen);
  }

  parent[inst.sy][inst.sx] = {x:inst.sx, y:inst.sy};
  seen[inst.sy][inst.sx] = true;
  let remaining:Coord[] = [];

  exploreNeighbors(inst.sx, inst.sy, min_dist_turn_start, g, parent, seen, remaining)

  let seenNodes = remaining.map((t)=>{return {x:t.x,y:t.y}}); // used for display seen nodes (debug)

  // main loop
  while ( !seen[inst.ty][inst.tx] && remaining.length > 0 ) {
    let u = remaining.shift(); // pop first elt of remaining
    if ( u !== undefined ) {
        seenNodes.push({x:u.x,y:u.y});
        exploreNeighbors(u.x, u.y, min_dist_turn, g, parent, seen, remaining)
    }
  }

  // fill res
  res.push({x:inst.tx,y:inst.ty});
  let cond = true;
  do {
    let e:Coord = res[res.length-1];
    if ( e.x == -1 || e.y == -1 ) return [];
    cond = parent[e.y][e.x] != e;
    if ( cond ) {
      res.push(parent[e.y][e.x]);
    }
  } while(cond);
  res.pop();
  res.reverse();
  return res;
}


/**
  if a path is too close a chamber, we make it farther
*/
function correctPathFurtherChambers(p:Coord[], nodes:Step2Node[], chip:Chip) {
  let epsilon = 0.1;
  let res = [];
  let res_a,res_b;
  for ( let i = 1; i < p.length-1 ; i++ ) { // iterate over segments of channel
    let a = p[i];
    let b = p[i+1];
    res_a = {x:a.x,y:a.y};
    res_b = {x:b.x,y:b.y};
    if ( Math.abs(a.x-b.x) < epsilon ) { // vertical
      let b_min = 0;
      let b_max = chip.w;
      for ( let n of nodes ) {
        // look if one node is parallel to the channel, update b_min & b_max for where the channel can be
        if ( isOverlap(n.y, n.y+n.h, Math.min(p[i].y,p[i+1].y), Math.max(p[i].y,p[i+1].y)) ) {
          // update b_min
          // test left side of node
          if ( n.x < a.x && n.x > b_min )
            b_min = n.x;
          // test right side of node
          if ( n.x+n.w < a.x && n.x+n.w > b_min )
            b_min = n.x+n.w;
          // update b_max
          // test left side of node
          if ( n.x > a.x && n.x < b_max )
            b_max = n.x;
          // test right side of nodes
          if ( n.x+n.w > a.x && n.x+n.w < b_max )
            b_max = n.x+n.w;
        }
      }
      // if b_max-b_min too small : center it
      if ( b_max-b_min < 2*minDistChamber() ) {
        p[i].x = (b_max+b_min)/2;
        p[i+1].x = (b_max+b_min)/2;
      }
      else {
        // else: if b_max or b_min too close, move it
        if ( a.x - b_min < minDistChamber() ) {
          p[i].x = b_min+minDistChamber()
          p[i+1].x = b_min+minDistChamber();
        }
        if ( b_max-a.x < minDistChamber() ) {
          p[i].x = b_max-minDistChamber();
          p[i+1].x = b_max-minDistChamber();
        }
      }
    } else { // horizontal
      let b_min = 0;
      let b_max = chip.h;
      for ( let n of nodes ) {
        // check if x intervals are joint
        if ( isOverlap(n.x, n.x+n.w, Math.min(p[i].x,p[i+1].x), Math.max(p[i].x,p[i+1].x)) ) {
          // update b_min
          // test top side of node
          if ( n.y < a.y && n.y > b_min )
            b_min = n.y;
          // test bottom side of node
          if ( n.y+n.h < a.y && n.y+n.h > b_min )
            b_min = n.y+n.h;
          // update b_max
          // test top side of node
          if ( n.y > a.y && n.y < b_max )
            b_max = n.y;
          // test bottom side of nodes
          if ( n.y+n.h > a.y && n.y+n.h < b_max )
            b_max = n.y+n.h;
        }
      }
      // if b_max-b_min too small (< 4*precision) : center it
      if ( b_max-b_min < 2*minDistChamber() ) {
        p[i].y = (b_max+b_min)/2;
        p[i+1].y = (b_max+b_min)/2;
      } else {
        // else: if b_max or b_min too close, move it
        if ( a.y - b_min < minDistChamber() ) {
          p[i].y = b_min+minDistChamber();
          p[i+1].y = b_min+minDistChamber();
        }
        if ( b_max-a.y < minDistChamber() ) {
          p[i].y = b_max-minDistChamber();
          p[i+1].y = b_max-minDistChamber();
        }
      }
    }
  }
}

/**
  centers channels to the center of chambers
*/
function correctPath(p:Coord[], n:Step2Node, c:Inlet) {

  let ncx = n.x+n.w/2;
  let ncy = n.y+n.h/2;
  let epsilon = 0.1;

  if ( p.length <= 1 ) return;
  else if (p.length == 2 ) { // case of direct line
    p[0].x = ncx;
    p[0].y = ncy;
    if ( Math.abs(p[0].x-p[1].x) > epsilon ) {// if horizontal
      p[1].y = ncy;
      c.y = ncy;
    } else { // if vertical
      p[1].x = ncx;
      c.x = ncx;
    }
  } else {
    // correct node
    if ( Math.abs(p[0].x-p[1].x) > 0 ) { // horizontal
      p[0].y = ncy;
      p[1].y = ncy;
      p[0].x = ncx;
    } else { // vertical
      p[0].x = ncx;
      p[1].x = ncx;
      p[0].y = ncy;
    }

    // correct chamber
    let i = p.length-2;
    let j = p.length-1;
    if ( Math.abs(p[i].x-p[j].x) > 0 ) { // horizontal
      p[i].y = c.y;
      p[j].y = c.y;
      p[j].x = c.x;
    } else { // vertical
      p[i].x = c.x;
      p[j].x = c.x;
      p[j].y = c.y;
    }
  }
}

/**
  returns a graph and path for a given routing problem
  parameters:
    - inlets: list of inlets
    - outlets: list of outlets
    - nodes: list of nodes
    - chip: dimentions of chip
    - route: direction for a given route
    - paths: list of previous drawn channels
*/
export function prepRouting(inlets:Inlet[], outlets:Outlet[], nodes:Step2Node[], chip:Chip,
route:Route, channels:Step3Channel[], minDistPunchHole:number, minDistChannel: number, previous_path?:Coord[]) {
  let paths = []
  if ( previous_path ) paths.push(previous_path)
  channels.forEach( (e) => {
    paths.push(e.in)
    paths.push(e.out)
  })

  // Construct an approximation graph for the routing problem
  let g = createGraph(inlets,outlets,nodes,chip,route,paths, minDistPunchHole, minDistChannel);

  // extracts start/end points for the routing problem
  let chamber = route.type=="inlet"?inlets[DataProvider.getNodeIdFromName(route.node)]:outlets[DataProvider.getNodeIdFromName(route.node)];
  let node = nodes[DataProvider.getNodeIdFromName(route.node)];
  let startingPoints = generateStartingPoints(chamber, node);

  // routing problem: find a path in the approximation graph
  let path = findPath(startingPoints, g);

  // multiply by precision
  for ( let a of path ) {
    a.x *= precision();
    a.y *= precision()
  }

  // corrects details in the path
  correctPathFurtherChambers(path, nodes, chip);
  correctPath(path, node, chamber);

  return {
    graph:g,
    path:path
  };
}
