/* eslint-disable */

import { Inlet, Outlet, Step2Node, Chip, ChannelDirection, Step3Channel } from "../../structures"
import { Rectangle, Circle, overlapRectangleCircle, rectanglesOverlap } from "../../geometry/Collisions"
import { Coord } from "../../geometry/Coord"
import { DataProvider } from "../../DataProvider"

/**
  Exports:
    - interface Route
    - interface PathInstance (coordinates of starting and ending nodes in graph)
    - createGraph ( create an approximation graph for a routing problem)
*/

function precision() {
  return DataProvider.getStep3Data().params.precision
}

function distBorder() {
  return DataProvider.getStep3Data().params.distBorder
}

function radius() {
  return DataProvider.getStep3Data().params.minDistChamber
  // return DataProvider.getStep3Data().params.minRadius
}


export interface Route {
  node:string,
  type:string,
  direction:ChannelDirection
};

export interface PathInstance {
  sy:number,
  sx:number,
  tx:number,
  ty:number
};


function inChipBounds(c:Coord, chip:Chip):boolean {
  if ( c.x >= 0 && c.y >= 0 && c.x < chip.w/precision() && c.y < chip.h/precision() )
    return true;
  return false;
}


/**
  returns true if rectangle a overlaps inlet/outlet bounding box (~1mm additionnal)
*/
function overlapRectCircle(a:Rectangle, b:Circle):boolean {
  return overlapRectangleCircle(a, {x: b.x, y: b.y, r: b.r})
}

/**
  Create an approximation graph of the chip for the routing problem
*/
function approximationGraph(inlets:Inlet[], outlets:Outlet[], nodes:Step2Node[], chip:Chip, minDistPunchHole:number):boolean[][] {
  let res = [];
  for ( let y = 0 ; y < chip.h ; y += precision() ) {
    let tmp = [];
    for ( let x = 0 ; x < chip.w ; x += precision() ) {
      tmp.push(true);
    }
    res.push(tmp);
  }

  let distNode = DataProvider.getStep3Data().params.minDistNode

  // mask positions near objects on the chip
  for ( let i in nodes ) {
    let bbox_node = {
      x:nodes[i].x-distNode,
      y:nodes[i].y-distNode,
      w:nodes[i].w+2*distNode,
      h:nodes[i].h+2*distNode
    };

    // for all possible positions, check if intersecting
    for ( let y = 0 ; y < res.length ; y++ ) {
      for ( let x = 0 ; x < res[y].length ; x++ ) {
        let tmp:Rectangle = {
          x:x*precision(),
          y:y*precision(),
          w:precision(),
          h:precision()
        };
        let inlet_extended = {x: inlets[i].x, y: inlets[i].y, r: inlets[i].r+minDistPunchHole}
        let outlet_extended = {x: outlets[i].x, y: outlets[i].y, r: outlets[i].r+minDistPunchHole}
        if (rectanglesOverlap(tmp,bbox_node) || overlapRectCircle(tmp,inlet_extended) || overlapRectCircle(tmp,outlet_extended)) {
          res[y][x] = false;
        }
      }
    }
  }

  return res;
}

/**
  Correct graph according to context of drawing a route
*/
export function createGraph(inlets:Inlet[], outlets:Outlet[], nodes:Step2Node[],
chip:Chip, route:Route, paths:Coord[][], minDistPunchHole:number, minDistChannel:number):boolean[][] {
  let res = approximationGraph(inlets, outlets, nodes, chip, minDistPunchHole);
  // free inlet/outlet
  let chamber;
  if ( route.type == "inlet" ) {
    chamber = inlets[DataProvider.getNodeIdFromName(route.node)];
  } else { // outlet
    chamber = outlets[DataProvider.getNodeIdFromName(route.node)];
  }
  // free chamber
  let nbAug = 1;
  for ( let i = Math.floor(-(chamber.r+minDistPunchHole)/precision())-nbAug ; i <= (chamber.r+minDistPunchHole)/precision()+nbAug ; i+=1 ) {
      let y1 = Math.floor(chamber.y/precision())+i;
      let x1 = Math.floor(chamber.x/precision());
      if ( inChipBounds({x:x1,y:y1}, chip) ) res[y1][x1] = true;
      let y2 = Math.floor(chamber.y/precision());
      let x2 = Math.floor(chamber.x/precision())+i;
      if ( inChipBounds({x:x2,y:y2}, chip) ) res[y2][x2] = true;
  }
  // free node
  let distNode = DataProvider.getStep3Data().params.minDistNode
  let nbMoreVerticesToClear = Math.ceil(distNode/precision())+1;
  let n = nodes[DataProvider.getNodeIdFromName(route.node)];
  if ( route.direction === ChannelDirection.Vertical ) {
    for ( let i = -nbMoreVerticesToClear; i <= n.h/precision()+nbMoreVerticesToClear ; i+=1 ) {
      let x = Math.floor((n.x/precision()+n.w/precision()/2));
      let y = Math.floor((n.y/precision()+i));
      if ( i >= 0 && i <= n.h/precision() ) {
        if ( inChipBounds({x:x+1,y:y}, chip) ) res[y][x+1] = false;
        if ( inChipBounds({x:x-1,y:y}, chip) ) res[y][x-1] = false;
      }
      if ( inChipBounds({x:x,y:y}, chip) ) res[y][x] = true;
    }
  } else { // horizontal
    for ( let i = -nbMoreVerticesToClear; i <= n.w/precision()+nbMoreVerticesToClear ; i+=1 ) {
      let x = Math.floor((n.x/precision()+i));
      let y = Math.floor((n.y/precision()+n.h/precision()/2));
      if ( i >= 0 && i <= n.w/precision() ) {
        if ( inChipBounds({x:x,y:y-1}, chip) ) res[y-1][x] = false;
        if ( inChipBounds({x:x,y:y+1}, chip) ) res[y+1][x] = false;
      }
      if ( inChipBounds({x:x,y:y}, chip) ) res[y][x] = true;
    }
  }

  // add paths to graph
  for ( let p of paths ) {

    let old = {x:-1,y:-1};
    for ( let c of p ) {
      let cx = Math.floor(c.x/precision());
      let cy = Math.floor(c.y/precision());
      if ( old.x == -1 ) {
        old.x = cx;
        old.y = cy;
      }
      else {
        let width_over = Math.floor(minDistChannel/precision())
        if ( old.x != cx ) { // if horizontal path
          for ( let i = Math.min(old.x,cx) ; i <= Math.max(old.x,cx) ; i++ ) {
            for ( let wid = -width_over ; wid <= width_over ; wid++ ) {
              res[cy+wid][i] = false;
            }
          }
        } else { // if vertical path
          for ( let i = Math.min(old.y,cy) ; i <= Math.max(old.y,cy) ; i++ ) {
            for ( let wid = -width_over ; wid <= width_over ; wid++ ) {
              res[i][cx+wid] = false;
            }
          }
        }
        old = {x:cx,y:cy};
      }
    }
  }

  // free center case
  res[Math.floor((n.y+n.h/2)/precision())][Math.floor((n.x+n.w/2)/precision())] = true;

  // make borders not available
  for ( var i = 0 ; i < res[0].length ; i++ ) {
    for ( var j = 0 ; j < Math.floor(distBorder()/precision()) ; j++) {
      res[j][i] = false;
      res[res.length-1-j][i] = false;
    }
  }
  for ( var i = 0 ; i < res.length ; i++ ) {
    for ( var j = 0 ; j < Math.floor(distBorder()/precision()) ; j++) {
      res[i][j] = false;
      res[i][res[0].length-1-j] = false;
    }
  }

  return res;
}
