# chip-designer

Semi-automatic brain-on-chip designer

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## TODO

- [ ] interate warnings if fabrication soft constraints are not respected.
- [ ] tuto to better grasp the software
- [X] red more flashy (step 2)
- [X] automatic surface computation if change number of neurons (step 1 - recomputed when pushing on "enter" while the text field is active)
- [X] inlet/outlet links (more transparent lines)
- [ ] maintaining states (retour arrière)
- [ ] step2 warning (add infotip to better explain how to solve the problem)
- [ ] add infotips with a "?" mark
- [ ] resize (try using gravity center)
- [X] width micro channels 50 -> 200 (réglage via interface des contraintes to be done later)
- [ ] warning when two chambers are too close